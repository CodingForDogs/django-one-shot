from django.apps import AppConfig


class BrainTwoAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "brain_two_app"
